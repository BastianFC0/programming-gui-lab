//Bastian Fernandez Cortez
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application {
    private RpsGame rps = new RpsGame();
    @Override
    public void start(Stage stage){
        Group root = new Group();

        //Scene is associated with container, dimensions
        Scene scene = new Scene(root, 650, 300);
        scene.setFill(Color.BLACK);

        //Associate scene to stage and show
        stage.setTitle("Rock Paper Scissors");
        stage.setScene(scene);
        stage.show();

        //Element creation
        VBox overall = new VBox();

        HBox buttons = new HBox();
        Button rockButton = new Button("rock");
        Button scissorsButton = new Button("scissors");
        Button paperButton = new Button("paper");

        HBox textFields = new HBox();
        TextField message = new TextField("Welcome!");
        message.setPrefWidth(200);
        TextField wins = new TextField("Wins: "+0);
        TextField losses = new TextField("Losses: "+0);
        TextField ties = new TextField("Ties: "+0);

        //Element appending
        textFields.getChildren().addAll(message, wins, losses, ties);
        buttons.getChildren().addAll(rockButton, scissorsButton, paperButton);
        overall.getChildren().addAll(buttons, textFields);
        root.getChildren().add(overall);

        //Action
        RpsChoice actionRock = new RpsChoice(message, wins, losses, ties, "rock", rps);
        rockButton.setOnAction(actionRock);
        RpsChoice actionScissors = new RpsChoice(message, wins, losses, ties, "scissors", rps);
        scissorsButton.setOnAction(actionScissors);
        RpsChoice actionPaper = new RpsChoice(message, wins, losses, ties, "paper", rps);
        paperButton.setOnAction(actionPaper);
    }
    public static void main(String[] args){
        Application.launch(args);
    }
}
