//Bastian Fernandez Cortez
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> {
    private TextField message;
    private TextField wins;
    private TextField losses;
    private TextField ties;
    private String choice;
    private RpsGame rps;
    public RpsChoice(TextField message, TextField wins, TextField losses,
    TextField ties, String choice, RpsGame rps){
        this.message = message;
        this.wins = wins;
        this.losses = losses;
        this.ties = ties;
        this.choice = choice;
        this.rps = rps;
    }
    @Override
    public void handle(ActionEvent e){
        String result = this.rps.playRound(this.choice);
        this.message.setText(result);
        this.wins.setText("Wins: "+this.rps.getWins());
        this.losses.setText("Losses: "+this.rps.getLosses());
        this.ties.setText("Ties: "+this.rps.getTies());
    }
}
