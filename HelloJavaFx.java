import javafx.application.*;
import javafx.scene.paint.*;
import javafx.scene.*;
import javafx.stage.*;

public class HelloJavaFx extends Application { 	

    @Override
   public void start(Stage stage) { 
           //container and layout
           Group root = new Group(); 

           //scene is associated with container, dimensions
           Scene scene = new Scene(root, 400, 300); 
           scene.setFill(Color.BLACK);

           //associate scene to stage and show
           stage.setTitle("Hello JavaFX"); 
           stage.setScene(scene); 
           stage.show(); 
       } 
          public static void main(String[] args) {
                   Application.launch(args);
          }    
} 
