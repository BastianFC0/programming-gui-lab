//Bastian Fernandez Cortez
import java.util.Random;

public class RpsGame{
    private int wins;
    private int ties;
    private int losses;
    public RpsGame(){
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
    }
    public int getWins(){
        return this.wins;
    }
    public int getTies(){
        return this.ties;
    }
    public int getLosses(){
        return this.losses;
    }
    public String playRound(String choice){
        Random random = new Random();
        int cpuNumber = random.nextInt(3);
        String[] arraySign = {"rock", "paper","scissors"};
        String cpuChoice = arraySign[cpuNumber];
        if(!choice.equals(arraySign[0])&&!choice.equals(arraySign[1])&&!choice.equals(arraySign[2])){
            throw new IllegalArgumentException("The choice must be between rock, paper and scissors");
        }
        if(choice.equals(arraySign[0])){
            if(cpuChoice.equals(arraySign[0])){
                ++this.ties;
               return "Computer plays "+arraySign[0]+", it's a draw"; 
            }
            if(cpuChoice.equals(arraySign[1])){
                ++this.losses;
                return "Computer plays "+arraySign[1]+", computer wins";
            }
            if(cpuChoice.equals(arraySign[2])){
                ++this.wins;
                return "Computer plays "+arraySign[2]+", computer looses";
            }
        }
        if(choice.equals(arraySign[1])){
            if(cpuChoice.equals(arraySign[0])){
                ++this.wins;
               return "Computer plays "+arraySign[0]+", computer looses"; 
            }
            if(cpuChoice.equals(arraySign[1])){
                ++this.ties;
               return "Computer plays "+arraySign[1]+", it's a draw"; 
            }
            if(cpuChoice.equals(arraySign[2])){
                ++this.losses;
               return "Computer plays "+arraySign[2]+", computer wins"; 
            }
        }
        if(choice.equals(arraySign[2])){
            if(cpuChoice.equals(arraySign[0])){
                ++this.losses;
               return "Computer plays "+arraySign[0]+", computer wins"; 
            }
            if(cpuChoice.equals(arraySign[1])){
                ++this.wins;
               return "Computer plays "+arraySign[1]+", computer looses"; 
            }
            if(cpuChoice.equals(arraySign[2])){
                ++this.ties;
               return "Computer plays "+arraySign[2]+", it's a draw"; 
            }
        }
        return "error";
    }
}